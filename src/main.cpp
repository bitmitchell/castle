#include "main.h"
#include "main_menu.h"
#include "debugging/log.h"
#include "state.h"
#include "debugging/frame_query.h"
#include "utility/command_line.h"

#include <SDL.h>

namespace {
	const double update_rate = 60.0;
	const double update_period = 1.0 / update_rate;
}


void app_state::apply_graphics_settings() {
	int w, h;
	SDL_GetWindowSize(window, &w, &h);
	if (w != window_width || h != window_height) {
		SDL_SetWindowSize(window, window_width, window_height);
		LOG("Set window size to %%%x%%%", window_width, window_height);
	}

	SDL_SetWindowFullscreen(window, window_fullscreen ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0);
	LOG("Window fullscreen state: %%%", window_fullscreen);
}


void shutdown(std::string error) {
	if (!error.empty()) {
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", error.c_str(), nullptr);
#if defined(_DEBUG)
		__debugbreak();
#endif
	}

	SDL_Quit();
	exit(0);
}


int main(int argc, char** argv) {

	ASSERT(SDL_Init(SDL_INIT_EVERYTHING) == 0, btl::format("SDL_Init failure (%%%)", SDL_GetError()));

	app_state app;

	app.window_width = get_parameter<int>("-w", argc, argv, 1024);
	app.window_height = get_parameter<int>("-h", argc, argv, 576);
	app.window_fullscreen = get_parameter<bool>("-f", argc, argv, false);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	app.window = SDL_CreateWindow("Castle",
								  SDL_WINDOWPOS_UNDEFINED,
								  SDL_WINDOWPOS_UNDEFINED,
								  app.window_width,
								  app.window_height,
								  SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	ASSERT(app.window, btl::format("SDL_CreateWindow returned nullptr (%%%)", SDL_GetError()));

	app.context = SDL_GL_CreateContext(app.window);
	ASSERT(app.context, btl::format("SDL_GL_CreateContext failed (%%%)", SDL_GetError()));

	glewExperimental = true;
	ASSERT(glewInit() == GLEW_OK, btl::format("glewInit failed"));

	app.apply_graphics_settings();

	SDL_GL_SetSwapInterval(0);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    LOG("OpenGL Version: %%%", glGetString(GL_VERSION));
    LOG("Shading Version: %%%", glGetString(GL_SHADING_LANGUAGE_VERSION));
	LOG("SDL and GLEW initialized.");

	state::ptr current_state = std::make_shared<main_menu>(app);

	double current_time = app.time.get_time();
	double time_accumulator = 0.0f;

	frame_query frame_timer;

	for (;;) {
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
				case SDL_QUIT:
					shutdown();
					break;
				case SDL_KEYDOWN:
					app.key_press(event.key.keysym.sym);
					break;
				case SDL_KEYUP:
					app.key_release(event.key.keysym.sym);
                    break;
                case SDL_MOUSEMOTION:
                    app.mouse_move(event.motion.x, event.motion.y);
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    app.mouse_click(event.button.button, event.button.x, event.button.y);
                    break;
                case SDL_MOUSEBUTTONUP:
                    app.mouse_release(event.button.button, event.button.x, event.button.y);
                    break;
                case SDL_WINDOWEVENT:
                    switch (event.window.event) {
                        case SDL_WINDOWEVENT_RESIZED:
                            app.window_width = event.window.data1;
                            app.window_height = event.window.data2;
                            glViewport(0, 0, app.window_width, app.window_height);
                            app.window_resized(app.window_width, app.window_height);
                            break;
                    }
                    break;
			}
		}

		double t = app.time.get_time();
		double frame_time = t - current_time;
		if (frame_time > 0.25) {
			frame_time = 0.25;
		}
		current_time = t;

		time_accumulator += frame_time;

		while (time_accumulator > update_period) {
			time_accumulator -= update_period;

			double start = app.time.get_time();
			current_state = current_state->update(app, float(1.0 / update_rate));
			app.update_time = (float)(app.time.get_time() - start);
		}

		frame_timer.start();
		current_state->draw(app);
		frame_timer.end();

		app.render_time = frame_timer.get_time();


		SDL_GL_SwapWindow(app.window);
	}

	return 0;
}
