#pragma once

#include <GL/glew.h>
#include <cstdint>
#include <cassert>
#include <vector>
#include <algorithm>
#include <string>
#include <unordered_map>

#include "../utility/file.h"
#include "../maths.h"

#pragma warning( disable : 4505 )

enum shader_stage {
	vertex = GL_VERTEX_SHADER,
	fragment = GL_FRAGMENT_SHADER
};


/*
	A collection of helper functions to make code tidier
*/

// Get the orthographic transform matrix for the current viewport
static mat4 screen_projection() {
	int viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);
	return ortho((float)viewport[0], (float)viewport[2], (float)viewport[1], (float)viewport[3]);
}


/*
	The buffer wraps over the OpenGL buffer type. This can be used for things like vertex buffers and element array buffers when rendering geometery.
*/
template <GLenum type>
struct buffer {
	GLuint object;
	GLenum usage;
	uint32_t size;

	buffer(GLenum usage = GL_STATIC_DRAW) : usage(usage) {
		glCreateBuffers(1, &object);
	}

	~buffer() {
		glDeleteBuffers(1, &object);
	}

	buffer(const buffer&) = delete;
	void operator=(const buffer&) = delete;

	buffer(buffer&& rhs) {
		std::swap(object, rhs.object);
		std::swap(usage, rhs.usage);
		std::swap(size, rhs.size);
	}

	void operator=(buffer&& rhs) {
		std::swap(object, rhs.object);
		std::swap(usage, rhs.usage);
		std::swap(size, rhs.size);
	}

	void bind() const {
		glBindBuffer(type, object);
	}

	static void bind_null() {
		glBindBuffer(type, 0);
	}

	template <class T>
	void set_data(const T* data, uint32_t count) {
		size = count;
		bind();
		glBufferData(type, sizeof(T) * count, data, usage);
	}

	template <class T>
	void set_data(const std::vector<T>& data) {
		set_data(data.data(), (uint32_t)data.size());
	}

	template <class T, int N>
	void set_data(const T(&data)[N]) {
		set_data(data, N);
	}
};


typedef buffer<GL_ARRAY_BUFFER> array_buffer;
typedef buffer<GL_ELEMENT_ARRAY_BUFFER> element_buffer;


/*
	An array_object contains information about all of the separate array_buffer's that are used to define each vertex.
*/
struct array_object {
	GLuint object;
	GLuint buffer_count;

private:
	std::vector<const array_buffer*> buffers;
public:

	array_object() : buffer_count(0) {
		glCreateVertexArrays(1, &object);
	}

	~array_object() {
		glDeleteVertexArrays(1, &object);
	}

	array_object(const array_object&) = delete;
	void operator=(const array_object&) = delete;

	array_object(array_object&& rhs) {
		std::swap(object, rhs.object);
		std::swap(buffer_count, rhs.buffer_count);
		std::swap(buffers, rhs.buffers);
	}

	void operator=(array_object&& rhs) {
		std::swap(object, rhs.object);
		std::swap(buffer_count, rhs.buffer_count);
		std::swap(buffers, rhs.buffers);
	}

	void bind() {
		glBindVertexArray(object);
	}

	static void bind_null() {
		glBindVertexArray(0);
	}

	void bind_buffer(const array_buffer& b, GLenum data_type, int data_elements) {
		buffers.push_back(&b);
		glEnableVertexAttribArray(buffer_count);
		b.bind();
		glVertexAttribPointer(buffer_count++, data_elements, data_type, GL_FALSE, 0, nullptr);
	}

	void bind_buffer_int(const array_buffer& b, GLenum data_type, int data_elements) {
		buffers.push_back(&b);
		glEnableVertexAttribArray(buffer_count);
		b.bind();
		glVertexAttribIPointer(buffer_count++, data_elements, data_type, 0, nullptr);
	}

	void draw(GLenum primitives = GL_TRIANGLES) {
		assert(buffers.size() > 0 && "Must be at least one buffer in the array");
		if (buffers[0]->size) {
			glDrawArrays(primitives, 0, buffers[0]->size);
		}
	}
};


struct texture_2d {
	GLuint object;

	texture_2d() {
		glGenTextures(1, &object);
	}

	~texture_2d() {
		glDeleteTextures(1, &object);
	}

	texture_2d(const texture_2d&) = delete;
	void operator=(const texture_2d&) = delete;

	texture_2d(texture_2d&& rhs) {
		std::swap(object, rhs.object);
	}

	void operator=(texture_2d&& rhs) {
		std::swap(object, rhs.object);
	}

	void bind() const {
		glBindTexture(GL_TEXTURE_2D, object);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}

	template <class T>
	void set_data(const T* data, uint32_t width, uint32_t height, GLenum format, GLenum type) {
		bind();
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, type, data);
	}

	template <class T>
	void set_data(const std::vector<T>& data, uint32_t width, uint32_t height, GLenum format, GLenum type) {
		set_data(data.data(), width, height, format, type);
	}
};


template <shader_stage stage>
struct shader {
	GLuint object;

	shader() {
		object = glCreateShader(stage);
	}

	~shader() {
		glDeleteShader(object);
	}

	shader(const shader&) = delete;
	void operator=(const shader&) = delete;

	shader(shader&& rhs) {
		std::swap(object, rhs.object);
	}

	void operator=(shader&& rhs) {
		std::swap(object, rhs.object);
	}

	bool has_compiled() const {
		GLint status;
		glGetShaderiv(object, GL_COMPILE_STATUS, &status);
		return status == 1;
	}

	void set_source(const std::string& source) {
		GLint length = (GLint)source.size();
		const char* src = source.c_str();
		glShaderSource(object, 1, &src, &length);
		glCompileShader(object);

		glGetShaderiv(object, GL_INFO_LOG_LENGTH, &length);

		if (length > 1) {
			std::vector<char> info(length);
			glGetShaderInfoLog(object, length, &length, &info[0]);
			printf("Shader log: %s\n", info.data());
		}
		assert(has_compiled() && "Error compiling GLSL shader");
	}
};


template <class T>
void uniform(GLuint location, const T& value);


struct shader_program {
	GLuint object;
	std::unordered_map<std::string, GLuint> uniforms;

private:
	uint8_t texture_index;
public:

	shader_program() : texture_index(0) {
		object = glCreateProgram();
	}

	~shader_program() {
		glDeleteProgram(object);
	}

	shader_program(const shader_program&) = delete;
	void operator=(const shader_program&) = delete;

	shader_program(shader_program&& rhs) {
		std::swap(object, rhs.object);
	}

	void operator=(shader_program&& rhs) {
		std::swap(object, rhs.object);
	}

	void bind() {
		glUseProgram(object);
		texture_index = 0;
	}

	void set_shaders(const shader<vertex>& vertex, const shader<fragment>& fragment) {
		glAttachShader(object, vertex.object);
		glAttachShader(object, fragment.object);

		glLinkProgram(object);

		GLint status, length;
		glGetProgramiv(object, GL_LINK_STATUS, &status);
		glGetProgramiv(object, GL_INFO_LOG_LENGTH, &length);

		if (length > 1) {
			std::vector<char> info(length);
			glGetProgramInfoLog(object, length, &length, &info[0]);
			printf("Program log: %s\n", info.data());
		}
		assert(status && "Error linking GLSL shader program");

		GLint count = 0;
		glGetProgramiv(object, GL_ACTIVE_UNIFORMS, &count);

		for (int i = 0; i < count; ++i) {
			char name[256];
			GLsizei size;
			GLenum type;
			glGetActiveUniform(object, i, 256, &length, &size, &type, name);

			uniforms[name] = glGetUniformLocation(object, name);
		}
	}

	template <class T>
	void set_uniform(const std::string& name, const T& value) {
		bind();
		uniform(uniforms[name], value);
	}

	void set_texture(const std::string& name, const texture_2d& texture) {
        set_uniform<GLint>(name, texture_index);
		glActiveTexture(GL_TEXTURE0 + texture_index);
		texture.bind();
        texture_index++;
	}
};


template <shader_stage stage>
const shader<stage>& get_shader_source(const std::string& name, const std::string& source) {
	static std::unordered_map<std::string, shader<stage>> cache;

	shader<stage>& s = cache[name];
	if (!s.has_compiled()) {
		s.set_source(source);
	}
	return s;
}


template <shader_stage stage>
const shader<stage>& get_shader(const std::string& filename) {
	return get_shader_source<stage>(filename, read_file(filename));
}


// Template out the glUniform functions

template<>
void uniform<vec2>(GLuint location, const vec2& value);

template<>
void uniform<vec3>(GLuint location, const vec3& value);

template<>
void uniform<vec4>(GLuint location, const vec4& value);

template<>
void uniform<vec2i>(GLuint location, const vec2i& value);

template<>
void uniform<vec3i>(GLuint location, const vec3i& value);

template<>
void uniform<vec4i>(GLuint location, const vec4i& value);

template<>
void uniform<mat4>(GLuint location, const mat4& value);

template<>
void uniform<int>(GLuint location, const int& value);
