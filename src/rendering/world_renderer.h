#pragma once
#include "../components/object.h"
#include "rendering.h"
#include "shape_batch.h"
#include "../map.h"
#include "../utility/filter.h"
#include "../main.h"


class world_renderer {
	map& world;
	shape_batch shape;

	std::vector<mesh> world_meshes;
	shader_program world_program;
	texture_2d block_atlas;

	void draw_world(app_state& app, vec2 view_position);
	void draw_actors(app_state& app, vec2 view, object_collection& objects);

public:

	world_renderer(map& world);

	void draw(app_state& app, vec2 view_position, object_collection& objects);
};