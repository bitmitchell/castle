#pragma once

#include "rendering.h"

struct mesh {
	array_object object;
	std::vector<array_buffer> buffers;

	mesh() { }
	mesh(mesh&& other) {
		std::swap(object, other.object);
		std::swap(buffers, other.buffers);
	}

	void operator=(mesh&& other) {
		std::swap(object, other.object);
		std::swap(buffers, other.buffers);
	}
};
