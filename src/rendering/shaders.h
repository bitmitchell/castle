#pragma once
#include <string>


namespace {

	// Basic position and color shader
	std::string vertex_color = R"(
#version 330
layout(location=0) in vec2 position;
layout(location=1) in vec4 color;
uniform mat4 transform;
out vec4 frag_color;
void main() {
	frag_color = color;
	gl_Position = transform * vec4(position, 0.0, 1.0);
})";

	std::string fragment_color = R"(
#version 330
in vec4 frag_color;
out vec4 color;
void main() {
    color = frag_color;
})";

	// Basic position and uv shader
	std::string vertex_texture = R"(
#version 330
layout(location=0) in vec2 position;
layout(location=1) in int texture_index;
uniform mat4 transform;
uniform ivec2 atlas_size;
out vec2 frag_uv;
void main() {
	vec2 offsets[6] = vec2[] ( vec2(0, 0), vec2(1, 0), vec2(0, 1),
							   vec2(1, 0), vec2(1, 1), vec2(0, 1));

	int id = gl_VertexID % 6;
	float tx = float(texture_index % atlas_size.x);
	float ty = float(texture_index / atlas_size.y);

	frag_uv = vec2((float(offsets[id].x) + tx) / float(atlas_size.x), 
				   (float(offsets[id].y) + ty) / float(atlas_size.y));
	gl_Position = transform * vec4(position, 0.0, 1.0);
})";

	std::string fragment_texture = R"(
#version 330
uniform sampler2D atlas;
in vec2 frag_uv;
out vec4 color;
void main() {
    color = texture(atlas, frag_uv);
})";

	// Font shader, inputs are Position, Color, and UV.
	std::string vertex_font = R"(
#version 330
layout(location=0) in vec3 position;
layout(location=1) in vec4 color;
layout(location=2) in vec2 uv;
uniform mat4 transform;
out vec4 frag_color;
out vec2 frag_uv;
void main() {
	gl_Position = transform * vec4(position, 1.0);
	frag_color = color;
	frag_uv = uv;
})";

	std::string fragment_font = R"(
#version 330
uniform sampler2D font_atlas;
in vec4 frag_color;
in vec2 frag_uv;
out vec4 color;
void main() {
	float f = texture(font_atlas, frag_uv).x;
	color = vec4(1.0, 1.0, 1.0, f) * frag_color;
})";

}
