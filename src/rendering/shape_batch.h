#pragma once
#include "rendering.h"
#include "shaders.h"

struct shape_batch {
    array_buffer buffer_position, buffer_color;
    array_object geometry;

    std::vector<vec2> positions;
    std::vector<vec4> colors;

    shader_program program;

    shape_batch() : buffer_position(GL_DYNAMIC_DRAW), buffer_color(GL_DYNAMIC_DRAW) {
        program.set_shaders(get_shader_source<vertex>("color", vertex_color),
            get_shader_source<fragment>("color", fragment_color));

        geometry.bind();
        geometry.bind_buffer(buffer_position, GL_FLOAT, vec2::size);
        geometry.bind_buffer(buffer_color, GL_FLOAT, vec4::size);
    }

    void line(vec2 a, vec2 b, vec4 c) {
        positions.push_back(a);
        positions.push_back(b);

        colors.push_back(c);
        colors.push_back(c);
    }

	void box_outline(vec2 a, vec2 b, vec4 c) {
		vec2 tr = { b.x(), a.y() };
		vec2 bl = { a.x(), b.y() };
		line(a, tr, c);
		line(tr, b, c);
		line(b, bl, c);
		line(a, bl, c);
	}

    void draw(GLenum primitives, const mat4& transform) {
        buffer_position.set_data(positions);
        buffer_color.set_data(colors);

        positions.clear();
        colors.clear();

		geometry.bind();
        program.bind();

        program.set_uniform("transform", screen_projection() * transform);

        geometry.draw(primitives);
    }
};
