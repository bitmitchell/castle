#include "world_renderer.h"
#include "../components/common.h"
#include "../debugging/log.h"
#include <stb_image.h>

struct rgba {
	uint8_t r, g, b, a;
};

world_renderer::world_renderer(map& world) : world(world) {
	world_program.set_shaders(get_shader_source<vertex>("texture", vertex_texture), get_shader_source<fragment>("texture", fragment_texture));

	int blocks_w = 8, blocks_h = 8;
	std::vector<rgba> data(blocks_w * blocks_h * block_texture_size * block_texture_size);
	int i = 0;

	for (auto& id_info : world.block_types) {
		int w, h, comp;
		rgba* texture = (rgba*)stbi_load(id_info.second.texture_file.c_str(), &w, &h, &comp, 4);

		ASSERT(texture && w % block_texture_size == 0 && h == block_texture_size, "Block texture has incorrect dimensions");

		id_info.second.texture_position = i;
		id_info.second.texture_count = w / block_texture_size;

		for (int j = 0; j < id_info.second.texture_count; ++j) {
			int left = (i % blocks_w) * block_texture_size;
			int top = (i / blocks_h) * block_texture_size;
			for (int y = 0; y < block_texture_size; ++y) {
				for (int x = 0; x < block_texture_size; ++x) {
					data[(x + left + (y + top) * (blocks_w * block_texture_size))] = texture[x + j * block_texture_size + y * w];
				}
			}
			++i;
		}

		stbi_image_free(texture);
	}

	block_atlas.set_data(data, blocks_w * block_texture_size, blocks_h * block_texture_size, GL_RGBA, GL_UNSIGNED_BYTE);

	world_meshes.resize(world.chunks_width * world.chunks_height);
}


void world_renderer::draw_world(app_state& app, vec2 view_pos) { 
	mat4 view_transform = translate(-view_pos.x(), -view_pos.y(), 0);

	world_program.bind();
	world_program.set_uniform("transform", screen_projection() * view_transform);
	world_program.set_texture("atlas", block_atlas);
	world_program.set_uniform("atlas_size", vec2i{ 8, 8 });


	int min_x = std::max(int(view_pos.x() / (chunk_size * block_size)), 0);
	int max_x = std::min(min_x + int(app.window_width / (chunk_size * block_size)) + 2, world.chunks_width);
	int min_y = std::max(int(view_pos.y() / (chunk_size * block_size)), 0);
	int max_y = std::min(min_y + int(app.window_height / (chunk_size * block_size)) + 2, world.chunks_height);

	for (int y = min_y; y < max_y; ++y) {
		for (int x = min_x; x < max_x; ++x) {
			int index = x + y * world.chunks_width;
			if (world.chunks[index].modified) {
				world_meshes[index] = world.chunks[index].get_mesh(x, y, world);
			}

			mesh& m = world_meshes[index];
			m.object.bind();
			m.object.draw(GL_TRIANGLES);
		}
	}
}


void world_renderer::draw_actors(app_state& app, vec2 view_pos, object_collection& objects) {
	UNUSED(app);

	for (object& obj : objects) {
		transform* frame = obj.get<transform>();
		debug_outline* outline = obj.get<debug_outline>();
		if (frame && outline) {
			shape.box_outline(frame->position * block_size - vec2{ outline->left, outline->top },
							  frame->position * block_size + vec2{ outline->right, outline->bottom },
							  { 0, 1, 0, 1 });
		}
	}


	shape.draw(GL_LINES, translate(-view_pos.x(), -view_pos.y(), 0));
}


void world_renderer::draw(app_state& app, vec2 view, object_collection& objects) {
	draw_world(app, view);
	draw_actors(app, view, objects);
}
