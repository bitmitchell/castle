#pragma once
#include "rendering.h"
#include "font_8x8.h"
#include "shaders.h"


class font {
	static const int characters = 128;
	static const int glyph_width = 8, glyph_height = 8;
	static const int atlas_width = 128, atlas_height = 128;
	static const int glyphs_per_row = atlas_width / glyph_width;

	array_buffer buffer_pos, buffer_col, buffer_uv;
	array_object geometry;
	texture_2d texture_font;
	shader_program shader;

	void load_font() {
		std::vector<uint8_t> image;
		image.resize(atlas_width * atlas_height);

		for (int i = 0; i < characters; ++i) {
			uint8_t* glyph = detail::font_8x8[i];

			int x = i % glyphs_per_row;
			int y = i / glyphs_per_row;

			for (int r = 0; r < glyph_height; ++r) {
				for (int c = 0; c < glyph_width; ++c) {
					int pixel_x = x * glyph_width + c;
					int pixel_y = y * glyph_height + r;

					image[pixel_x + pixel_y * atlas_width] = (glyph[r] & (1 << c) ? 255 : 0);
				}
			}
		}

		texture_font.set_data(image, atlas_width, atlas_height, GL_RED, GL_UNSIGNED_BYTE);
	}


	int buffer_string(const std::string& string, const vec4& color) {
		std::vector<vec3> positions;
		std::vector<vec4> colors;
		std::vector<vec2> uvs;

		int x = 0, y = 0;

		positions.reserve(string.size() * 6);
		colors.reserve(string.size() * 6);
		uvs.reserve(string.size() * 6);

		for (char c : string) {

			int glyph_x = (c % glyphs_per_row) * glyph_width;
			int glyph_y = (c / glyphs_per_row) * glyph_height;

			positions.push_back({ x, y, 0 });
			positions.push_back({ x + glyph_width, y, 0 });
			positions.push_back({ x, y + glyph_height, 0 });

			positions.push_back({ x + glyph_width, y, 0 });
			positions.push_back({ x + glyph_width, y + glyph_height, 0 });
			positions.push_back({ x, y + glyph_height, 0 });

			colors.push_back(color);
			colors.push_back(color);
			colors.push_back(color);

			colors.push_back(color);
			colors.push_back(color);
			colors.push_back(color);

			uvs.push_back({ float(glyph_x) / float(atlas_width), float(glyph_y) / float(atlas_height) });
			uvs.push_back({ float(glyph_x + glyph_width) / float(atlas_width), float(glyph_y) / float(atlas_height) });
			uvs.push_back({ float(glyph_x) / float(atlas_width), float(glyph_y + glyph_height) / float(atlas_height) });

			uvs.push_back({ float(glyph_x + glyph_width) / float(atlas_width), float(glyph_y) / float(atlas_height) });
			uvs.push_back({ float(glyph_x + glyph_width) / float(atlas_width), float(glyph_y + glyph_height) / float(atlas_height) });
			uvs.push_back({ float(glyph_x) / float(atlas_width), float(glyph_y + glyph_height) / float(atlas_height) });

			// TODO: Handle a tab character here
			if (c == '\n') {
				x = 0;
				y += glyph_height;
			} else {
				x += glyph_width;
			}
		}

		buffer_pos.set_data(positions);
		buffer_col.set_data(colors);
		buffer_uv.set_data(uvs);

		return y + glyph_height;
	}


public:

	static const int line_height = glyph_height;

	font() : buffer_pos(GL_DYNAMIC_DRAW), buffer_col(GL_DYNAMIC_DRAW), buffer_uv(GL_DYNAMIC_DRAW) {
		load_font();
		shader.set_shaders(get_shader_source<vertex>("font", vertex_font), get_shader_source<fragment>("font", fragment_font));

		geometry.bind();
		geometry.bind_buffer(buffer_pos, GL_FLOAT, vec3::size);
		geometry.bind_buffer(buffer_col, GL_FLOAT, vec4::size);
		geometry.bind_buffer(buffer_uv, GL_FLOAT, vec2::size);
	}

	int draw_string(const std::string& string, int x, int y, int size = 1, const vec4& color = { 1, 1, 1, 1 }) {
		int line = buffer_string(string, color) * size;

		mat4 transform = screen_projection() *
			translate(float(x), float(y), 0.0f) *
			scale(float(size), float(size));

		geometry.bind();
		shader.bind();
		shader.set_uniform("transform", transform);
		shader.set_texture("font_atlas", texture_font);

		geometry.draw();
		return y + line;
	}

	static vec2i measure_string(const std::string& string, int size = 1) {
		int w = 0;
		int h = 0;

		int x = 0, y = 8;

		for (char c : string) {
			if (c == '\n') {
				x = 0;
				y += glyph_height;
			} else {
				x += glyph_width;
			}

			w = std::max(w, x);
			h = std::max(h, y);
		}

		return{ w * size, h * size };
	}

    static font& instance() {
        static font fnt;
        return fnt;
    }
};
