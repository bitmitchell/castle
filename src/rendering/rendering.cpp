#include "rendering.h"


template<>
void uniform<vec2>(GLuint location, const vec2& value) {
	glUniform2fv(location, 1, value.data.data());
}

template<>
void uniform<vec3>(GLuint location, const vec3& value) {
	glUniform3fv(location, 1, value.data.data());
}

template<>
void uniform<vec4>(GLuint location, const vec4& value) {
	glUniform4fv(location, 1, value.data.data());
}

template<>
void uniform<vec2i>(GLuint location, const vec2i& value) {
	glUniform2iv(location, 1, value.data.data());
}

template<>
void uniform<vec3i>(GLuint location, const vec3i& value) {
	glUniform3iv(location, 1, value.data.data());
}

template<>
void uniform<vec4i>(GLuint location, const vec4i& value) {
	glUniform4iv(location, 1, value.data.data());
}


template<>
void uniform<mat4>(GLuint location, const mat4& value) {
	glUniformMatrix4fv(location, 1, GL_TRUE, value.data.data());
}

template<>
void uniform<int>(GLuint location, const int& value) {
	glUniform1i(location, value);
}
