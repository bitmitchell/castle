#pragma once
#include "../components/object.h"
#include "../components/common.h"
#include "../map.h"


class walking_physics : public component {
public:

	enum input_state {
		input_none = 0x00,
		input_left = 0x01,
		input_right = 0x02,
		input_jump = 0x04,
	};

	bool is_grounded;
	float top, left, bottom, right;
	int input;

	walking_physics(float top, float left, float bottom, float right) : top(top), left(left), bottom(bottom), right(right), is_grounded(false), input(input_none) { }
};


class physics {

	map& world;

	void handle_walker_input(transform* frame, walking_physics* walking, float dt);
	void collide_walker(transform* frame, walking_physics* walking);

public:

	physics(map& world) : world(world) { }

	void update(object_collection& objects, float dt);

	static vec2 collide_boxes(vec2 a, vec2 size_a, vec2 b, vec2 size_b);

};