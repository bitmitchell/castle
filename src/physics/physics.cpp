#include "physics.h"
#include "../components/common.h"

namespace {
	const float gravity = 50;
}

namespace walker {
	const float max_fall_speed = 40;
	const float max_speed = 17;
	const float acceleration = 0.99f;
	const float jump_height = 3.5f;
	const float jump_speed = std::sqrt(jump_height * gravity * 2);
	const float step_size = 1.01f;
}


vec2 physics::collide_boxes(vec2 a, vec2 size_a, vec2 b, vec2 size_b) {
	float dx = (size_a.x() + size_b.x()) * 0.5f - std::abs(a.x() - b.x());
	float dy = (size_a.y() + size_b.y()) * 0.5f - std::abs(a.y() - b.y());

	if (dx > 0 && dy > 0) {
		if (dx < dy) {
			return vec2{ (a.x() > b.x() ? dx : -dx), 0 };
		} else {
			return vec2{ 0, (a.y() > b.y() ? dy : -dy) };
		}
	}
	return{ };
}


void physics::update(object_collection& objects, float dt) {
	for (object& obj : objects) {
		transform* frame = obj.get<transform>();
		walking_physics* walking = obj.get<walking_physics>();

		if (frame) {
			frame->position += frame->velocity * dt;
		}

		if (frame && walking) {
			handle_walker_input(frame, walking, dt);
			collide_walker(frame, walking);
		}
	}
}


void physics::handle_walker_input(transform* frame, walking_physics* walking, float dt) {
	frame->velocity.y() += gravity * dt;
	frame->velocity.y() = std::min(frame->velocity.y(), walker::max_fall_speed);

	float target_x_speed = 0;
	if (walking->input & walking_physics::input_right) {
		target_x_speed += walker::max_speed;
	}
	if (walking->input & walking_physics::input_left) {
		target_x_speed -= walker::max_speed;
	}

	if (walking->input & walking_physics::input_jump && walking->is_grounded) {
		frame->velocity.y() = -walker::jump_speed;
	}

	frame->velocity.x() = lerp(frame->velocity.x(),
							   target_x_speed,
							   1.0f - std::pow(1.0f - walker::acceleration, dt));
}


void physics::collide_walker(transform* frame, walking_physics* walking) {
	walking->is_grounded = false;

	float left = frame->position.x() - walking->left;
	float right = frame->position.x() + walking->right;
	float top = frame->position.y() - walking->top;
	float bottom = frame->position.y() + walking->bottom;

	int min_x = (int)std::floor(left);
	int max_x = (int)std::ceil(right);
	int min_y = (int)std::floor(top);
	int max_y = (int)std::ceil(bottom);

	float width = walking->left + walking->right;
	float height = walking->bottom + walking->top;
	vec2 body_middle = { float(walking->right - walking->left) / 2,
						 float(walking->bottom - walking->top) / 2 };

	for (int x = min_x; x <= max_x; ++x) {
		for (int y = min_y; y <= max_y; ++y) {
			if (world(x, y) != invalid_block) {
				vec2 c = collide_boxes(frame->position + body_middle, vec2{ width, height },
									   vec2{ x, y } + vec2{ 0.5f, 0.5f }, vec2{ 1, 1 });

				if (c.length_squared() > 0) {
					if (c.y() == 0 && y >= frame->position.y() + walking->bottom - walker::step_size &&
						world.is_region_clear(left, float(y) - height, right, float(y - 1))) {
						frame->position.y() = float(y) - walking->bottom;
						frame->velocity.y() = 0;
						walking->is_grounded = true;
					} else {
						frame->position += c;
						vec2 cn = c.normalized();
						float v = frame->velocity.dot(cn);
						if (v < 0) {
							frame->velocity -= cn * v;
						}

						if (c.y() < 0) {
							walking->is_grounded = true;
						}
					}
				}
			}
		}
	}
}
