#pragma once

#include <string>


template <class T>
T get_parameter(const std::string& name, int argc, char** argv, T def = T()) {
	for (int i = 0; i < argc; ++i) {
		if (name == argv[i]) {
			std::stringstream ss;
			T value;
			ss << argv[i + 1];
			ss >> value;
			return value;
		}
	}
	return def;
}

template <>
bool get_parameter<bool>(const std::string& name, int argc, char** argv, bool def) {
	for (int i = 0; i < argc; ++i) {
		if (name == argv[i]) {
			return true;
		}
	}
	return def;
}