#pragma once


template <class T = float>
struct filter_exp {
    T state;
    float weight;

    filter_exp(float persistence = 0.9f) : weight(persistence), state() {}

    void add(T value) {
        state = state * weight + value * (1 - weight);
    }

    T get() {
        return state;
    }
};
