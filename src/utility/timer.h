#pragma once

#include <chrono>

typedef std::chrono::high_resolution_clock clock_source;

struct timer {
	clock_source::time_point start;

	timer() {
		reset();
	}

	void reset() {
		start = clock_source::now();
	}

	double get_time() {
		clock_source::time_point now = clock_source::now();
		clock_source::duration dt = now - start;
		return double(std::chrono::duration_cast<std::chrono::microseconds>(dt).count()) / 1e6;
	}
};