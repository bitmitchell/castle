#pragma once


#include <fstream>
#include <string>


static std::string read_file(const std::string& filename) {
	std::ifstream ifs(filename);
	return std::string ((std::istreambuf_iterator<char>(ifs)),
						(std::istreambuf_iterator<char>()));
}