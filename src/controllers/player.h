#pragma once
#include "../input.h"

class player_controller {
	keyboard_state keys;

public:

	player_controller(app_state& app) : keys(app) { }

	void update(object_collection& objects) {
		for (object& obj : objects) {
			if (obj.has<player_view, walking_physics>()) {
				walking_physics* walking = obj.get<walking_physics>();
				walking->input = walking_physics::input_none;
				if (keys(SDLK_a)) {
					walking->input |= walking_physics::input_left;
				}
				if (keys(SDLK_d)) {
					walking->input |= walking_physics::input_right;
				}
				if (keys(SDLK_SPACE)) {
					walking->input |= walking_physics::input_jump;
				}

				break;
			}
		}
	}
};