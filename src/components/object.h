#pragma once
#include <vector>
#include <memory>
#include <algorithm>


class object;


class component {
public:
	virtual ~component() { }
};



class object {
	std::vector<std::unique_ptr<component>> components;

public:

	object() { }

	object(object&& other) {
		std::swap(components, other.components);
	}

	void operator=(object&& other) {
		std::swap(components, other.components);
	}

	template <class T, class ...Args>
	T* add(Args&&... args) {
		components.emplace_back(new T(args...));
		return get<T>();
	}

	template <class T>
	T* get() {
		for (auto& ptr : components) {
			T* c = dynamic_cast<T*>(ptr.get());
			if (c) {
				return c;
			}
		}
		return nullptr;
	}

	template <class T>
	bool has() {
		return get<T>() != nullptr;
	}

	template <class T, class S, class ...U>
	bool has() {
		return has<T>() && has<S, U...>();
	}

	template <class T>
	void remove() {
		components.erase(std::remove_if(components.begin(), components.end(), [] (const std::unique_ptr<component>& ptr) {
			return dynamic_cast<T*>(ptr.get()) != nullptr;
		}), components.end());
	}
};



typedef std::vector<object> object_collection;
