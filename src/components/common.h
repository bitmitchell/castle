#pragma once
#include "object.h"
#include "../maths.h"



class transform : public component {
public:
	vec2 position, velocity;

	transform(vec2 p = vec2()) : position(p) { }
};


class debug_outline : public component {
public:
	float top, left, bottom, right;

	debug_outline(float t, float l, float b, float r) : top(t), left(l), bottom(b), right(r) { }
};


class player_view : public component { };