#include "main_menu.h"
#include "rendering/font.h"
#include "playground.h"


main_menu::main_menu(app_state& app) {
    UNUSED(app);
	LOG("Main menu created");

    commands.padding = 5;
    commands.add<button>("PLAY", 2, [&]() {
        next_state.reset(new playground(app));
    });
    commands.add<button>("QUIT", 2, []() {
        shutdown();
    });

    mouse_move.event = [this](int x, int y) {
        commands.mouse_move({ x, y });
    };
    mouse_click.event = [this](int button, int x, int y) {
        if (button == SDL_BUTTON_LEFT) {
            commands.mouse_click({ x, y });
        }
    };
    mouse_release.event = [this](int button, int x, int y) {
        if (button == SDL_BUTTON_LEFT) {
            commands.mouse_release({ x, y });
        }
    };

    app.mouse_move.attach(&mouse_move);
    app.mouse_click.attach(&mouse_click);
    app.mouse_release.attach(&mouse_release);
}


state::ptr main_menu::update(app_state&, float) {
	return  (next_state ? next_state : shared_from_this());
}


void main_menu::draw(app_state& app) {
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	font& fnt = font::instance();
	fnt.draw_string("CASTLE", 50, 50, 5);

	commands.position = { 20, app.window_height - commands.get_size().y() - 20 };
	commands.draw();
}
