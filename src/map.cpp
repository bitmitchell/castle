#include "map.h"
#include "rendering/rendering.h"


mesh map::chunk::get_mesh(int chunk_x, int chunk_y, map& world) {
	int offset_x = chunk_x * chunk_size;
	int offset_y = chunk_y * chunk_size;
	vec2 offset = vec2{ offset_x, offset_y } * block_size;
	mesh m;

	array_buffer pos_buffer, tex_buffer;

	std::vector<vec2> positions;
	std::vector<int> texture_indices;

	for (int y = 0; y < chunk_size; ++y) {
		for (int x = 0; x < chunk_size; ++x) {
			block& b = blocks[x + y * chunk_size];
			if (b.type != invalid_block) {
				vec2 tl = vec2{ x, y } *block_size;
				vec2 tr = vec2{ x + 1, y } *block_size;
				vec2 bl = vec2{ x, y + 1 } *block_size;
				vec2 br = vec2{ x + 1, y + 1 } *block_size;

				positions.push_back(tl + offset);
				positions.push_back(tr + offset);
				positions.push_back(bl + offset);

				positions.push_back(tr + offset);
				positions.push_back(br + offset);
				positions.push_back(bl + offset);

				int texture = world.block_types[b.type].texture_position + b.variation;
				texture_indices.push_back(texture);
				texture_indices.push_back(texture);
				texture_indices.push_back(texture);

				texture_indices.push_back(texture);
				texture_indices.push_back(texture);
				texture_indices.push_back(texture);
			}
		}
	}

	pos_buffer.set_data(positions);
	tex_buffer.set_data(texture_indices);

	m.buffers.push_back(std::move(pos_buffer));
	m.buffers.push_back(std::move(tex_buffer));

	m.object.bind();

	m.object.bind_buffer(m.buffers[0], GL_FLOAT, vec2::size);
	m.object.bind_buffer_int(m.buffers[1], GL_INT, 1);

	modified = false;

	return m;
}
