#pragma once
#include <string>
#include <format.h>
#include <GL/glew.h>
#include <SDL.h>
#include <event.h>
#include "utility/timer.h"

#define UNUSED(var) ((void)var)

struct app_state {
	SDL_Window* window;
	SDL_GLContext context;

	float update_time, render_time;

	int window_width, window_height;
	bool window_fullscreen;

	timer time;

	btl::event<SDL_Keycode> key_press, key_release;
    btl::event<int, int> mouse_move, window_resized;
    btl::event<int, int, int> mouse_click, mouse_release;

	void apply_graphics_settings();
};

extern void shutdown(std::string error = std::string());
