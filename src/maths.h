#pragma once
#include <array>

template <class T, int N>
struct vector {
	static const int size = N;
	std::array<T, N> data;

	vector(T v) {
		for (int i = 0; i < N; ++i) {
			data[i] = v;
		}
	}

	vector(const vector& other) {
		data = other.data;
	}

	template <class ...U>
	vector(U... v) : data{ { static_cast<T>(v)... } } { }

	template <class U = T>
	inline typename std::enable_if<N >= 1 && std::is_same<U, T>::value, U>::type& x() { return data[0]; }
	template <class U = T>
	inline typename std::enable_if<N >= 2 && std::is_same<U, T>::value, U>::type& y() { return data[1]; }
	template <class U = T>
	inline typename std::enable_if<N >= 3 && std::is_same<U, T>::value, U>::type& z() { return data[2]; }
	template <class U = T>
	inline typename std::enable_if<N >= 4 && std::is_same<U, T>::value, U>::type& w() { return data[3]; }

	template <class U = T>
	inline typename std::enable_if<N >= 1 && std::is_same<U, T>::value, U>::type& r() { return data[0]; }
	template <class U = T>
	inline typename std::enable_if<N >= 2 && std::is_same<U, T>::value, U>::type& g() { return data[1]; }
	template <class U = T>
	inline typename std::enable_if<N >= 3 && std::is_same<U, T>::value, U>::type& b() { return data[2]; }
	template <class U = T>
	inline typename std::enable_if<N >= 4 && std::is_same<U, T>::value, U>::type& a() { return data[3]; }

	// Const access
	template <class U = T>
	inline typename std::enable_if<N >= 1 && std::is_same<U, T>::value, U>::type x() const { return data[0]; }
	template <class U = T>
	inline typename std::enable_if<N >= 2 && std::is_same<U, T>::value, U>::type y() const { return data[1]; }
	template <class U = T>
	inline typename std::enable_if<N >= 3 && std::is_same<U, T>::value, U>::type z() const { return data[2]; }
	template <class U = T>
	inline typename std::enable_if<N >= 4 && std::is_same<U, T>::value, U>::type w() const { return data[3]; }

	template <class U = T>
	inline typename std::enable_if<N >= 1 && std::is_same<U, T>::value, U>::type r() const { return data[0]; }
	template <class U = T>
	inline typename std::enable_if<N >= 2 && std::is_same<U, T>::value, U>::type g() const { return data[1]; }
	template <class U = T>
	inline typename std::enable_if<N >= 3 && std::is_same<U, T>::value, U>::type b() const { return data[2]; }
	template <class U = T>
	inline typename std::enable_if<N >= 4 && std::is_same<U, T>::value, U>::type a() const { return data[3]; }

	inline T& operator()(int i) {
		return data[i];
	}

	inline T operator()(int i) const {
		return data[i];
	}

	inline vector& operator+=(const vector& r) {
		for (int i = 0; i < N; ++i) {
			data[i] += r.data[i];
		}
		return *this;
	}

	inline vector& operator-=(const vector& r) {
		for (int i = 0; i < N; ++i) {
			data[i] -= r.data[i];
		}
		return *this;
	}

	inline vector& operator*=(const vector& r) {
		for (int i = 0; i < N; ++i) {
			data[i] *= r.data[i];
		}
		return *this;
	}

	inline vector& operator/=(const vector& r) {
		for (int i = 0; i < N; ++i) {
			data[i] /= r.data[i];
		}
		return *this;
	}

	inline vector operator-() const {
		vector v;
		for (int i = 0; i < N; ++i) {
			v.data[i] = -data[i];
		}
		return v;
	}

	inline vector& operator+=(T r) {
		for (int i = 0; i < N; ++i) {
			data[i] += r;
		}
		return *this;
	}

	inline vector& operator-=(T r) {
		for (int i = 0; i < N; ++i) {
			data[i] -= r;
		}
		return *this;
	}

	inline vector& operator*=(T r) {
		for (int i = 0; i < N; ++i) {
			data[i] *= r;
		}
		return *this;
	}

	inline vector& operator/=(T r) {
		for (int i = 0; i < N; ++i) {
			data[i] /= r;
		}
		return *this;
	}

	template <int M>
	inline typename std::enable_if<M == N, vector>::type cross(const vector<T, M>& r) const {
		return vector{
			y() * r.z() - z() * r.y(),
			z() * r.x() - x() * r.z(),
			x() * r.y() - y() * r.x()
		};
	}

	inline vector normalized() const {
		return *this / length();
	}

	inline T dot(const vector& r) const {
		T sum = 0;
		for (int i = 0; i < N; ++i) {
			sum += data[i] * r.data[i];
		}
		return sum;
	}

	inline T length_squared() const {
		return dot(*this);
	}

	inline T length() const {
		return std::sqrt(length_squared());
	}
};


template <class T, int N>
vector<T, N> operator+(vector<T, N> a, const vector<T, N>& b) {
	return a += b;
}

template <class T, int N>
vector<T, N> operator-(vector<T, N> a, const vector<T, N>& b) {
	return a -= b;
}

template <class T, int N>
vector<T, N> operator*(vector<T, N> a, const vector<T, N>& b) {
	return a *= b;
}

template <class T, int N>
vector<T, N> operator/(vector<T, N> a, const vector<T, N>& b) {
	return a /= b;
}

template <class T, int N>
vector<T, N> operator+(vector<T, N> a, T b) {
	return a += b;
}

template <class T, int N>
vector<T, N> operator-(vector<T, N> a, T b) {
	return a -= b;
}

template <class T, int N>
vector<T, N> operator*(vector<T, N> a, T b) {
	return a *= b;
}

template <class T, int N>
vector<T, N> operator/(vector<T, N> a, T b) {
	return a /= b;
}


typedef vector<float, 2> vec2;
typedef vector<float, 3> vec3;
typedef vector<float, 4> vec4;

typedef vector<int, 2> vec2i;
typedef vector<int, 3> vec3i;
typedef vector<int, 4> vec4i;

typedef vector<double, 2> vec2d;
typedef vector<double, 3> vec3d;
typedef vector<double, 4> vec4d;


template <class T, int R, int C>
struct matrix {
	std::array<T, R * C> data;

	matrix() {
		for (int j = 0; j < R; ++j) {
			for (int i = 0; i < C; ++i) {
				at(j, i) = static_cast<T>(i == j ? 1 : 0);
			}
		}
	}

	template <int R2, int C2>
	matrix(const matrix<T, R2, C2>& other) {
		static_assert(R2 <= R && C2 <= C, "Matrix must be of equal or lesser dimensions");

		for (int j = 0; j < R; ++j) {
			for (int i = 0; i < C; ++i) {
				if (i < C2 && j < R2) {
					at(j, i) = other.at(j, i);
				} else {
					at(j, i) = static_cast<T>(i == j ? 1 : 0);
				}
			}
		}
	}

	template <class ...U>
	matrix(U... v) : data{ static_cast<T>(v)... } {}

	T& at(int row, int col) {
		return data[row * C + col];
	}

	T at(int row, int col) const {
		return data[row * C + col];
	}

};


template <class T, int R, int C>
vector<T, R> operator*(const matrix<T, R, C>& m, const vector<T, C>& v) {
	vector<T, R> o;
	for (int i = 0; i < R; ++i) {
		o.data[i] = T(0);
		for (int j = 0; j < C; ++j) {
			o.data[i] += m.at(i, j) * v(j);
		}
	}
	return o;
}


template <class T, int R, int O, int C>
matrix<T, R, C> operator*(const matrix<T, R, O>& a, const matrix<T, O, C>& b) {
	matrix<T, R, C> out;

	for (int r = 0; r < R; ++r) {
		for (int c = 0; c < C; ++c) {
			out.at(r, c) = T(0);
			for (int i = 0; i < O; ++i) {
				out.at(r, c) += a.at(r, i) * b.at(i, c);
			}
		}
	}

	return out;
}


typedef matrix<float, 2, 2> mat2;
typedef matrix<float, 3, 3> mat3;
typedef matrix<float, 4, 4> mat4;


struct quaternion {
	float x, y, z, w;

	quaternion() : x(0), y(0), z(0), w(1) { }
	quaternion(float angle, const vec3& axis) { 
		float r = std::sin(angle / 2.0f);
		w = std::cos(angle / 2.0f);
		x = axis.x() * r;
		y = axis.y() * r;
		z = axis.z() * r;
	}
};


static quaternion operator*(const quaternion& l, const quaternion& r) {
	quaternion q;
	q.w = l.w * r.w - l.x * r.x - l.y * r.y - l.z * r.z;
	q.x = l.w * r.x + l.x * r.w + l.y * r.z - l.z * r.y;
	q.y = l.w * r.y + l.y * r.w + l.z * r.x - l.x * r.z;
	q.z = l.w * r.z + l.z * r.w + l.x * r.y - l.y * r.x;
	return q;
}


static vec3 operator*(const vec3& v, const quaternion& q) {
	vec3 u = { q.x, q.y, q.z };
	float s = q.w;
	return u * 2.0f * u.dot(v) + v * (s * s - u.dot(u)) + u.cross(v) * 2.0f * s;
}


static mat4 ortho(float left, float right, float top, float bottom, float min = 0.0f, float max = 1.0f) {
	float w = 1 / (right - left);
	float h = 1 / (top - bottom);
	float d = 1 / (max - min);

	mat4 m;
	m.at(0, 0) = 2 * w;
	m.at(1, 1) = 2 * h;
	m.at(2, 2) = 1 * d;
	m.at(0, 3) = (right + left) * -w;
	m.at(1, 3) = (top + bottom) * -h;
	m.at(2, 3) = max / (max + min);
	return m;
}


static mat4 zflip() {
	mat4 mat;
	mat.at(2, 2) = -1.0f;
	mat.at(2, 3) = 1.0f;
	return mat;
}


static mat4 perspective(float fov, float aspect, float near, float far) {
	float t = std::tan(fov / 2);
	mat4 mat;
	mat.at(0, 0) = near / (aspect * t);
	mat.at(1, 1) = near / t;
	mat.at(2, 2) = (far) / (far - near);
	mat.at(2, 3) = (far * near) / (near - far);
	mat.at(3, 2) = 1;
	mat.at(3, 3) = 0;
	return zflip() * mat;
}


static mat4 look_at(vec3 position, vec3 target, vec3 up) {
	vec3 z = (target - position).normalized();
	vec3 x = up.cross(z).normalized();
	vec3 y = z.cross(x).normalized();

	mat4 mat;
	mat.at(0, 0) = x.x();
	mat.at(0, 1) = x.y();
	mat.at(0, 2) = x.z();
	mat.at(0, 3) = -x.dot(position);

	mat.at(1, 0) = y.x();
	mat.at(1, 1) = y.y();
	mat.at(1, 2) = y.z();
	mat.at(1, 3) = -y.dot(position);

	mat.at(2, 0) = z.x();
	mat.at(2, 1) = z.y();
	mat.at(2, 2) = z.z();
	mat.at(2, 3) = -z.dot(position);

	return mat;
}


static mat4 scale(float x = 1.0f, float y = 1.0f, float z = 1.0f) {
	mat4 m;
	m.at(0, 0) = x;
	m.at(1, 1) = y;
	m.at(2, 2) = z;
	return m;
}


static mat4 translate(float x, float y, float z) {
	mat4 m;
	m.at(0, 3) = x;
	m.at(1, 3) = y;
	m.at(2, 3) = z;
	return m;
}


static mat4 translate(const vec3& t) {
	return translate(t.x(), t.y(), t.z());
}


static mat3 rotate(float angle, const vec3& axis) {
	mat3 mat;
	float c = std::cos(angle);
	float s = std::sin(angle);

	mat.at(0, 0) = c + axis.x() * axis.x() * (1 - c);
	mat.at(0, 1) = axis.x() * axis.y() * (1 - c) - axis.z() * s;
	mat.at(0, 2) = axis.x() * axis.z() * (1 - c) + axis.y() * s;

	mat.at(1, 0) = axis.x() * axis.y() * (1 - c) + axis.z() * s;
	mat.at(1, 1) = c + axis.y() * axis.y() * (1 - c);
	mat.at(1, 2) = axis.y() * axis.z() * (1 - c) + axis.x() * s;

	mat.at(2, 0) = axis.x() * axis.z() * (1 - c) - axis.y() * s;
	mat.at(2, 1) = axis.y() * axis.z() * (1 - c) + axis.x() * s;
	mat.at(2, 2) = c + axis.z() * axis.z() * (1 - c);

	return mat;
}


static mat3 rotate(const quaternion& q) {
	mat3 m;
	m.at(0, 0) = 1.0f - 2.0f * (q.y * q.y + q.z * q.z);
	m.at(0, 1) = 2.0f * (q.x * q.y - q.w * q.z);
	m.at(0, 2) = 2.0f * (q.x * q.z + q.w * q.y);

	m.at(1, 0) = 2.0f * (q.x * q.y + q.w * q.z);
	m.at(1, 1) = 1.0f - 2.0f * (q.x * q.x + q.z * q.z);
	m.at(1, 2) = 2.0f * (q.y * q.z - q.w * q.x);

	m.at(2, 0) = 2.0f * (q.x * q.z - q.w * q.y);
	m.at(2, 1) = 2.0f * (q.y * q.z + q.w * q.x);
	m.at(2, 2) = 1.0f - 2.0f * (q.x * q.x + q.y * q.y);
	return m;
}

template <class T>
T lerp(T a, T b, float x) {
	return a * (1.0f - x) + b * x;
}