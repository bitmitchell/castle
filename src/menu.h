#pragma once
#include <memory>
#include <event.h>
#include "rendering/rendering.h"
#include "rendering/font.h"
#include "main.h"


struct menu_element {
	vec2i position;
	virtual vec2i get_size() { return vec2i(); }
	virtual void draw(vec2i p = { }) { UNUSED(p); }

	virtual bool mouse_click(vec2i m) = 0;
	virtual bool mouse_release(vec2i m) = 0;
	virtual bool mouse_move(vec2i m) = 0;
};


struct menu_list : public menu_element {
	std::vector<std::shared_ptr<menu_element>> elements;
	int padding;

	menu_list() : padding(0) { }

	template <class T, class... Args>
	void add(Args&&... args) {
		elements.emplace_back(new T(args...));
	}

	vec2i get_size() {
		vec2i size;
		for (auto& e : elements) {
			vec2i s = e->get_size();
			size.x() = std::max(size.x(), s.x());
			size.y() += s.y() + padding;
		}
		return size;
	}

	void draw(vec2i p = { }) {
		for (auto& e : elements) {
			e->draw(p + position);
			p.y() += e->get_size().y() + padding;
		}
	}

	bool mouse_move(vec2i m) {
		m -= position;
		for (auto& e : elements) {
			if (e->mouse_move(m)) {
				return true;
			}
			m.y() -= e->get_size().y() + padding;
		}
		return false;
	}

	bool mouse_click(vec2i m) {
		m -= position;
		for (auto& e : elements) {
			if (e->mouse_click(m)) {
				return true;
			}
			m.y() -= e->get_size().y() + padding;
		}
		return false;
	}

	bool mouse_release(vec2i m) {
		m -= position;
		for (auto& e : elements) {
			if (e->mouse_release(m)) {
				return true;
			}
			m.y() -= e->get_size().y() + padding;
		}
		return false;
	}
};



struct label : public menu_element {
	std::string text;
	int size;

	label(const std::string& text, int size) : text(text), size(size) { }

	vec2i get_size() {
		return font::measure_string(text, size);
	}

	virtual void draw(vec2i p = { }) {
		vec2i pos = p + position;
		font::instance().draw_string(text, pos.x(), pos.y(), size);
	}

	virtual bool mouse_move(vec2i m) {
		vec2i s = get_size();
		return m.x() < s.x() && m.y() < s.y() && m.x() >= 0 && m.y() >= 0;
	}

	virtual bool mouse_click(vec2i m) {
		vec2i s = get_size();
		return m.x() < s.x() && m.y() < s.y() && m.x() >= 0 && m.y() >= 0;
	}

	virtual bool mouse_release(vec2i m) {
		vec2i s = get_size();
		return m.x() < s.x() && m.y() < s.y() && m.x() >= 0 && m.y() >= 0;
	}
};


struct button : public label {
	std::function<void(void)> click;
	enum { idle, hovering, pressed } state;

	button(const std::string& text, int size, std::function<void(void)> click) : label(text, size), click(click), state(idle) { }

	void draw(vec2i p = { }) {
		vec4 colors[] = { { 0.7, 0.7, 0.7, 1 }, { 1, 1, 0.7, 1 }, { 0.4, 0.4, 0.4, 1 } };
		vec2i pos = p + position;
		font::instance().draw_string(text, pos.x(), pos.y(), size, colors[state]);
	}

	virtual bool mouse_move(vec2i m) {
		if (label::mouse_move(m)) {
			if (state == idle) {
				state = hovering;
			}
			return true;
		}
		if (state == hovering) {
			state = idle;
		}
		return false;
	}

	virtual bool mouse_click(vec2i m) {
		if (label::mouse_click(m)) {
			state = pressed;
			return true;
		}
		return false;
	}

	virtual bool mouse_release(vec2i m) {
		if (state == pressed) {
			state = idle;
			if (label::mouse_release(m)) {
				click();
				return true;
			}
		}
		return false;
	}
};
