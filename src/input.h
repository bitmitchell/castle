#pragma once
#include <array>
#include "main.h"

struct mouse_position : btl::listener<int, int> {
	int x, y;

	mouse_position(app_state& state) {
		state.mouse_move.attach(this);
	}

	void on_event(int mx, int my) {
		x = mx;
		y = my;
	}
};


struct keyboard_state {
	std::array<bool, 256> keys;
	btl::delegate_listener<SDL_Keycode> key_press, key_release;

	keyboard_state(app_state& app) {
		for (int i = 0; i < keys.size(); ++i) {
			keys[i] = false;
		}

		key_press.event = [&] (SDL_Keycode k) {
			if (k < keys.size()) {
				keys[k] = true;
			}
		};
		key_release.event = [&] (SDL_Keycode k) {
			if (k < keys.size()) {
				keys[k] = false;
			}
		};

		app.key_press.attach(&key_press);
		app.key_release.attach(&key_release);
	}

	bool operator()(SDL_Keycode key) {
		assert(key < keys.size() && "Cannot access key outside of keyboard size");
		return keys[key];
	}
};