#pragma once
#include <vector>
#include <array>
#include <cstdint>
#include <unordered_map>
#include "rendering/mesh.h"
#include "utility/json.h"
#include "utility/file.h"

typedef uint32_t block_type;

namespace {
	const float block_size = 16;
	const int chunk_size = 16;
	const block_type invalid_block = 0;
	const int block_texture_size = 8;
}

struct block_info {
	block_type id;
	std::string name;
	std::string texture_file;
	int texture_position, texture_count;
};


struct block {
	block_type type;
	int variation;

	block() : type(invalid_block), variation(0) { }
	block(block_type t, int v) : type(t), variation(v) { }
};


struct map {
	struct chunk {
		std::array<block, chunk_size * chunk_size> blocks;
		bool modified;

		block& operator()(int x, int y) {
			return blocks[x + y * chunk_size];
		}

		mesh get_mesh(int chunk_x, int chunk_y, map& world);

		chunk() : modified(true) { }
	};

	int chunks_width, chunks_height;

	std::vector<chunk> chunks;
	std::unordered_map<block_type, block_info> block_types;

	map(int cw, int ch, std::string block_data) : chunks_width(cw), chunks_height(ch) {
		chunks.resize(cw * ch);
		load_block_data(block_data);
	}

	block_type operator()(int x, int y) {
		int chunk_x = x / chunk_size;
		int chunk_y = y / chunk_size;
		if (x < 0 || chunk_x >= chunks_width || y < 0 || chunk_y >= chunks_height) {
			return invalid_block;
		}
		return chunks[chunk_x + chunk_y * chunks_width](x % chunk_size, y % chunk_size).type;
	}

	void set(int x, int y, block_type t) {
		int chunk_x = x / chunk_size;
		int chunk_y = y / chunk_size;
		if (x < 0 || chunk_x >= chunks_width || y < 0 || chunk_y >= chunks_height) {
			return;
		}
		chunks[chunk_x + chunk_y * chunks_width](x % chunk_size, y % chunk_size) = {
			t, rand() % block_types[t].texture_count
		};
		chunks[chunk_x + chunk_y * chunks_width].modified = true;
	}

	bool is_region_clear(int x1, int y1, int x2, int y2) {
		for (int i = x1; i <= x2; ++i) {
			for (int j = y1; j <= y2; ++j) {
				if (this->operator()(i, j) != invalid_block) {
					return false;
				}
			}
		}
		return true;
	}

	bool is_region_clear(float x1, float y1, float x2, float y2) {
		return is_region_clear((int)std::floor(x1), (int)std::floor(y1), (int)std::floor(x2), (int)std::floor(y2));
	}

	int get_width() const {
		return chunks_width * chunk_size;
	}

	int get_height() const {
		return chunks_height * chunk_size;
	}

	void load_block_data(std::string block_file) {
		typedef nlohmann::json json;
		json file = json::parse(read_file(block_file));
		assert(file.size() > 0 && "Error loading data/blocks.json");

		block_type free_id = invalid_block + 1;

		for (json& e : file) {
			block_info info;
			info.id = free_id++;
			info.name = e["name"].get<std::string>();
			info.texture_file = e["texture"].get<std::string>();
			block_types[info.id] = info;
		}
	}

	block_type get_block(std::string name) {
		return std::find_if(block_types.begin(), block_types.end(), [&] (const std::pair<block_type, block_info>& info) { return name == info.second.name; })->first;
	}
};
