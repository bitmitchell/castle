#include "playground.h"
#include "rendering/font.h"
#include "components/common.h"


float offset() {
	return 2.0f * float(rand()) / float(RAND_MAX) - 1.0f;
}


void playground::subdivide_height(std::vector<float>& height, int a, int b, float s) {
	if (b - a < 2) {
		return;
	}

	int m = (a + b) / 2;
	height[m] = (height[a] + height[b]) * 0.5f + offset() * s;
	subdivide_height(height, a, m, s * 0.5f);
	subdivide_height(height, m, b, s * 0.5f);
}


playground::playground(app_state& app) : world(50, 8, "data/blocks.json"),
										 physics_engine(world),
										 view_position(0.7f),
										 renderer(world),
										 player_input(app) {

	std::vector<float> height;
	height.resize(world.get_width(), (float)world.get_height() / 2.0f);

	subdivide_height(height, 0, world.get_width() / 2, (float)world.get_height() / 4);
	subdivide_height(height, world.get_width() / 2, (int)height.size() - 1, (float)world.get_height() / 4);

	block_type dirt = world.get_block("Dirt");
	block_type grass = world.get_block("Grass");
	for (int x = 0; x < world.get_width(); ++x) {
		for (int y = 0; y < world.get_width(); ++y) {
			if (y > height[x] && y - 1 <= height[x]) {
				world.set(x, y, grass);
			} else if (y > height[x]) {
				world.set(x, y, dirt);
			}
		}
	}

	float player_left = 0.9f;
	float player_right = 0.9f;
	float player_top = 0.9f;
	float player_bottom = 1.8f;

	object player;
	player.add<transform>(vec2{ world.get_width() / 2, world.get_height() / 2 - 5});
	player.add<debug_outline>(player_top * 16.0f, player_left * 16.0f, player_bottom * 16.0f, player_right * 16.0f);
	player.add<walking_physics>(player_top, player_left, player_bottom, player_right);
	player.add<player_view>();
	objects.push_back(std::move(player));

	object dummy;
	dummy.add<transform>(vec2{ world.get_width() / 2 + 3, world.get_height() / 2 - 5 });
	dummy.add<debug_outline>(16.0f, 16.0f, 32.0f, 16.0f);
	dummy.add<walking_physics>(1.0f, 1.0f, 2.0f, 1.0f);
	objects.push_back(std::move(dummy));

	click.event = [this, &app, dirt] (int button, int x, int y) {
		if (button == SDL_BUTTON_LEFT) {
			x -= app.window_width / 2;
			y -= app.window_height / 2;

			vec2 world_pos = view_position.get() + vec2{ x, y } / block_size;
			int block_x = int(world_pos.x());
			int block_y = int(world_pos.y());

			world.set(block_x, block_y, dirt);
		}
	};
	app.mouse_click.attach(&click);
}


state::ptr playground::update(app_state&, float dt) {
	physics_engine.update(objects, dt);
	player_input.update(objects);

	for (object& obj : objects) {
		if (obj.has<player_view, transform>()) {
			view_position.add(obj.get<transform>()->position);
		}
	}

	return shared_from_this();
}


void playground::draw(app_state& app) {
	glClearColor(0.39f, 0.58f, 0.93f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);


	vec2 view_pos = view_position.get() * block_size;
	view_pos -= vec2{ app.window_width / 2, app.window_height / 2 };
	view_pos.x() = std::floor(view_pos.x());
	view_pos.y() = std::floor(view_pos.y());

	renderer.draw(app, view_pos, objects);

    update_time.add(app.update_time * 1000.0f);
    render_time.add(app.render_time * 1000.0f);

	font::instance().draw_string(btl::format("Frame time: %%%ms\nUpdate time: %%%ms", render_time.get(), update_time.get()), 0, 0);
}
