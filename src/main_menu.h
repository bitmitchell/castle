#pragma once
#include "state.h"
#include "debugging/log.h"
#include "main.h"
#include "menu.h"

#include <event.h>


class main_menu : public state {
    menu_list commands;

    btl::delegate_listener<int, int> mouse_move;
    btl::delegate_listener<int, int, int> mouse_click, mouse_release;

    ptr next_state;

public:
	main_menu(app_state& app);
	ptr update(app_state& app, float dt);
	void draw(app_state& app);
};
