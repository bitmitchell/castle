#pragma once
#include "format.h"

#define ASSERT(cond, msg) { if (!(cond)) { shutdown(msg); }}
#define LOG(...) { printf("%s:%i\n> %s\n\n", __FILE__, __LINE__, btl::format(__VA_ARGS__).c_str()); }