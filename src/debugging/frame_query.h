#pragma once
#include <SDL.h>
#include <GL/glew.h>

struct frame_query {
	enum { running, waiting, finished } state;

	GLuint query;
	float time;

	frame_query() : time(0.0f), state(finished) {
		glGenQueries(1, &query);
	}

	~frame_query() {
		glDeleteQueries(1, &query);
	}

	void start() {
		if (state == finished) {
			glBeginQuery(GL_TIME_ELAPSED, query);
			state = running;
		}
	}

	void end() {
		if (state == running) {
			state = waiting;
			glEndQuery(GL_TIME_ELAPSED);
		}
	}

	float get_time() {
		if (state == waiting) {
			GLint available;
			glGetQueryObjectiv(query, GL_QUERY_RESULT_AVAILABLE, &available);
			if (available) {
				GLuint64 nanoseconds;
				glGetQueryObjectui64v(query, GL_QUERY_RESULT, &nanoseconds);
				time = float(nanoseconds) / 1e9f;
				state = finished;
			}
		}

		return time;
	}
};