#pragma once
#include "state.h"
#include "debugging/log.h"
#include "main.h"
#include "rendering/shape_batch.h"
#include "map.h"
#include "input.h"
#include "utility/filter.h"
#include "components/object.h"
#include "physics/physics.h"
#include "rendering/world_renderer.h"
#include "controllers/player.h"


class playground : public state {
	map world;

	btl::delegate_listener<int, int, int> click;

	filter_exp<float> update_time, render_time;
	filter_exp<vec2> view_position;

	physics physics_engine;
	world_renderer renderer;

	object_collection objects;

	player_controller player_input;

	void subdivide_height(std::vector<float>& height, int a, int b, float s);

	void draw_world(app_state& app, vec2 view);
	void draw_actors(app_state& app, vec2 view);

public:
	playground(app_state& app);
	ptr update(app_state& app, float dt);
	void draw(app_state& app);
};
