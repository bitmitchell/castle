#pragma once
#include <memory>

struct app_state;

// Virtual state class, the object it returns is the next state to be in, or nullptr to remain in the same state
class state : public std::enable_shared_from_this<state> {
public:
	typedef std::shared_ptr<state> ptr;
	virtual ~state() {}
	virtual ptr update(app_state&, float dt) = 0;
	virtual void draw(app_state&) = 0;
};
