cmake_minimum_required(VERSION 3.0 FATAL_ERROR)

project(castle)

set(CMAKE_CXX_STANDARD 11)

set(THIRDPARTY_DIR ${CMAKE_CURRENT_SOURCE_DIR}/thirdparty)
add_subdirectory(thirdparty)


include_directories(${SDL2_BINARY_DIR}/include
					${SDL2_SOURCE_DIR}/include
					${THIRDPARTY_DIR}/btl
					${THIRDPARTY_DIR}/glew
					${THIRDPARTY_DIR}/SimpleText)

# Use a set bin output folder
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/bin)

set(USERFILE_WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/bin)
set(USERFILE_COMMAND_ARGUMENTS "")

add_subdirectory(src)
